<!--
Updated 2021-2022 Tested on Windows, will only work with x64 on VS2017. 
-->

# Cross Platform SDL2 #

Based on year one Game Systems Fundamentals (CIS400x 2020-2021) this will be a single project for all projects. 

Was [this](https://github.com/aminosbh/sdl2-cmake-modules) for SDL2 integration, but this seems to be largely broken.  Moved to using [my fork](https://github.com/smu-sc-gj/sdl2-cmake-modules) which has some fixes. 

## How to Clone ##

Note: Make sure you use this rather than just cloning as there's a submodule in the repository for the CMake stuff. 

```git clone --recurse-submodules https://<USERNAME>@bitbucket.org/smu_sc_gj/crossplatformsdl2_2020.git```

## Guides ##

Windows  guide ... needs detail

### Windows 10 ###

Note: Make sure when choosing a compiler and toolchain you select x64, testing with x86 and VS2017 resutls in an error. 

**CMake & Project Generation**

1. Setup CMakeGUI, the source code directory should be set to the project root and the build directory below this. 
2. Set ```SDL2_DIR``` to the root directory of the SDL2 libraries (contains includes, lib etc). 
3. Set ```SDL2_IMAGE_DIR``` to the root directory of the SDL2 libraries (contains includes, lib etc). 
4. Configure
5. Generate project files (tested with VS2019 and VS2017 - see note above). 
6. Open the Visual Studio 2019 project. 
   
**Compile and Build**

1. Change the startup project to the one with the SDL code in it (in this e.g. Worksheet 1). 
2. Build the project
3. Build the INSTALL target
4. Debug or Run the project
   
**Note** Be sure to run run the INSTALL target to copy the compiled exe across to the debug directory (allong with the dlls and assets) so it runs from here. 
