set(APP Worksheet1)

file(GLOB GAME_FILES *.cpp)

IF (WIN32)
	add_executable(${APP} WIN32 ${GAME_FILES})
ELSE()
	add_executable(${APP} ${GAME_FILES})
ENDIF()

# Link SDL2
target_link_libraries(${APP}  LINK_PRIVATE SDL2::Main)

# Link SDL2_image
target_link_libraries(${APP}  LINK_PRIVATE SDL2::Image)

#Includes only for this target - these could possibly be for all targets but...
#target_include_directories (${APP} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../engine)

#-------------------
# INSTALL TARGET
#-------------------

message(STATUS "Current binary directory " ${CMAKE_CURRENT_BINARY_DIR})
message(STATUS "Project binary directory " ${PROJECT_BINARY_DIR})

# post-build copy for win32
if(WIN32 AND NOT MINGW)
	add_custom_command( TARGET ${APP} PRE_BUILD
		COMMAND if not exist .\\dist mkdir .\\dist )
	add_custom_command( TARGET ${APP} POST_BUILD
		COMMAND copy \"$(TargetPath)\" .\\dist )
endif(WIN32 AND NOT MINGW)

if(MINGW OR UNIX)
	set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/dist)
endif(MINGW OR UNIX)

install(TARGETS ${APP}
	CONFIGURATIONS Release RelWithDebInfo Debug
	RUNTIME DESTINATION .
	)

message(STATUS "Source path" ${CMAKE_SOURCE_DIR})
message(STATUS "Output path" ${CMAKE_INSTALL_PREFIX})

install(DIRECTORY ${CMAKE_SOURCE_DIR}/src/assets
	CONFIGURATIONS Release RelWithDebInfo Debug
	DESTINATION ./)
		
if(WIN32)
	# https://stackoverflow.com/questions/23950887/does-cmake-offer-a-method-to-set-the-working-directory-for-a-given-build-system
	# Ignored due to a VS issue https://developercommunity.visualstudio.com/content/problem/268817/debugger-no-longer-respects-localdebuggerworkingdi.html
	set_target_properties(${APP} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_INSTALL_PREFIX}")

	# Get all the SDL2 files! 
	file(GLOB_RECURSE SDL2_DLL_FILES  ${SDL2_PATH}/*.dll)
	file(TO_CMAKE_PATH "${SDL2_DLL_FILES}" SDL2_DLL_FILES_NATIVE)
	#message(STATUS "Original DLL Files:" ${SDL2_DLL_FILES})
	#message(STATUS "Native DLL Files:" ${SDL2_DLL_FILES_NATIVE})

	install(FILES ${SDL2_DLL_FILES_NATIVE}
		CONFIGURATIONS Release RelWithDebInfo Debug
		DESTINATION .
		)

	# Get all the SDL2_image files! 
	file(GLOB_RECURSE SDL2_IMAGE_DLL_FILES  ${SDL2_IMAGE_PATH}/*.dll)
	file(TO_CMAKE_PATH "${SDL2_IMAGE_DLL_FILES}"SDL2_IMAGE_DLL_FILES_NATIVE)

	install(FILES ${SDL2_IMAGE_DLL_FILES_NATIVE}
		CONFIGURATIONS Release RelWithDebInfo Debug
		DESTINATION .
		)

endif()