/*
Begning SDL - Simple SDL example

*/

//Include SDL library
//#include "SDL2/SDL.h"
#include "SDL.h"

int main( int argc, char* args[] )
{
     // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     playerTexture;

    const int SDL_OKAY = 0;

    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    int sdlStatus = SDL_Init(SDL_INIT_EVERYTHING);

    gameWindow = SDL_CreateWindow("Hello CIS4008",          // Window title
                              SDL_WINDOWPOS_UNDEFINED,  // X position
                              SDL_WINDOWPOS_UNDEFINED,  // Y position
                              800, 600,                 // width, height
                              SDL_WINDOW_SHOWN);        // Window flags

    // if the window creation succeeded create our renderer
    gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

    //Setup sprite
    temp = SDL_LoadBMP("assets/images/beefy-miracle.bmp");

    // NOTE: SDL is limted to loading BMPs
    //       We'll solve this problem later.

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    playerTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'image' now our playerTexture has been created
    SDL_FreeSurface(temp);

    //Draw stuff here.

    // 1. Clear the screen
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

    SDL_RenderClear(gameRenderer);

    // 2. Draw the player
    SDL_RenderCopy(gameRenderer, playerTexture, NULL, NULL);

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    //Pause to allow the image to be seen
    SDL_Delay( 10000 );

    //Clean up!
    SDL_DestroyTexture(playerTexture);
    SDL_DestroyRenderer(gameRenderer);
    SDL_DestroyWindow(gameWindow);

    //Shutdown SDL - clear up resorces etc. 
    SDL_Quit();

    return 0;
}
